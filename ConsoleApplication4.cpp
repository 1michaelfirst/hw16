﻿#include <iostream>
#include <time.h>

int getCurrentDay() {
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    return buf.tm_mday;
}

int main()
{
    const int N = 6;

    int day = getCurrentDay();

    std::cout << "Current day is " << day << std::endl;

    int dayLine = day % N;
    int sumOfDayLine = 0;

    std::cout << "Array " << N << "x" << N << ":" << std::endl;

    int numbers[N][N];

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            numbers[i][j] = i + j;

            std::cout << numbers[i][j] << " ";

            if (i == dayLine) {
                sumOfDayLine += numbers[i][j];
            }
        }

        std::cout << std::endl;
    }

    std::cout << "Sum of line " << dayLine << " (" << day << " % " << N << "): " << sumOfDayLine << std::endl;
}
